'use strict';
module.exports = function(app) {
 	var cubes = require('../controllers/cubeController');




    /*begins api routes*/
    app.get('/api', function(req, res) {
      res.json({ message: 'Welcome to the backendTest Rappi RESTful API' });
    });


  	app.route('/api/cubes')
    	.get(cubes.processInput)
      .post(cubes.processInput);



    
};  

