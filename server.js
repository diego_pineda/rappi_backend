var express = require('express'),
 	app = express(),
 	port = process.env.PORT || 3001;
	http = require('http'),
 	morgan = require('morgan'),

	
	//middleware
 	bodyParser = require('body-parser'),
 	expressValidator = require('express-validator');



app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressValidator());

// use morgan to log requests to the console
app.use(morgan('dev'));


var routes = require('./api/routes/apiRoutes');
routes(app);

app.get('/', function(req, res) {
    res.send('API is at http://localhost:' + port + '/api');
});

app.use(function(req, res) {
	res.status(404).send({url: req.originalUrl + ' not found'})
});



app.listen(port);
console.log('Welcome to the backendTest Rappi API server started on: ' + port);
